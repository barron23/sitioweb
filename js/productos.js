//Configuración firebase

  //Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-app.js";
  //import { getAnalytics } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-analytics.js";
  import { getFirestore, doc, getDoc, getDocs, collection } from "https://www.gstatic.com/firebasejs/9.4.0/firebase-firestore.js";
  import { getDatabase,onValue,ref,set,child,get,update,remove} from "https://www.gstatic.com/firebasejs/9.12.1/firebase-database.js";
  import { getStorage, ref as refS, uploadBytes, getDownloadURL} from "https://www.gstatic.com/firebasejs/9.12.1/firebase-storage.js";

  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  const firebaseConfig = {
    apiKey: "AIzaSyBGRaWBXGgITL5xJRwwwvYVIbmVb0x18wc",
    authDomain: "proweb-1-15711.firebaseapp.com",
    databaseURL: "https://proweb-1-15711-default-rtdb.firebaseio.com",
    projectId: "proweb-1-15711",
    storageBucket: "proweb-1-15711.appspot.com",
    messagingSenderId: "270825728421",
    appId: "1:270825728421:web:79a7a955d212a75453e04c",
    measurementId: "G-JKMP3B6JT6"
  };

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const db = getDatabase();
  var caja=document.getElementById('Imagen');


  function importe(){
    const db = getDatabase();
    const dbRef= ref(db, 'productos');
    onValue(dbRef,(snapshot) => {
        caja.innerHTML="";
        snapshot.forEach((childSnapshot) => {
         
            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();
            if(childData.status=="0"){
            caja.innerHTML= caja.innerHTML +"<div class='imagen2'>" +
             
            "<img src='"+childData.url+"' alt=''>" +"<br>"+
            "<p style='font-size: larger;'>" + childData.nombre +"<br>"+"Precio: $"+childData.precio+ "<br>"+"Contiene: "+ childData.descripcion +"</p>" + 
            "</div>" ;
        }
        });
        {
            onlyOnce: true
        }
    });
  }
window.onload(importe());
